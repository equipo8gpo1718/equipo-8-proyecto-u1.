
package tarea1;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 *
 * @author Donato
 */
public class InfoClientes {

    public InfoClientes(){
        marco();
    }
    
    public void marco(){
        JFrame f = new JFrame();
        f.setSize(500,500);
        f.setLocationRelativeTo(null);
        f.setTitle("Informacion de los clientes");
        DefaultListModel listaClients = new DefaultListModel();
        JList listaClientes = new JList(); 
        
        
        //Paneles que se van a utilizar en la forma:
        JPanel panelMayor = new JPanel(new BorderLayout());
        
        JPanel panelNorte = new JPanel();
        JPanel panelCentro = new JPanel();
        JPanel panelSur = new JPanel();
        JPanel panelEste = new JPanel();
        JPanel panelOeste = new JPanel();
        
        //Layout Managers a utilizar:
        FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER);
        panelNorte.setLayout(fl1);
        
        GridBagLayout gbl1 = new GridBagLayout();
        GridBagConstraints gbc1 = new GridBagConstraints();
        panelCentro.setLayout(gbl1);
        
        FlowLayout fl2 = new FlowLayout();
        panelSur.setLayout(fl2);
        
        GridLayout gl2 = new GridLayout(3, 0);
        panelEste.setLayout(gl2);
        
        GridLayout gl3 = new GridLayout(3, 0);
        panelOeste.setLayout(gl3);
        
        //Creamos componentes para panel Norte:
        JLabel lblTitulo = new JLabel("Rellene los campos para agregar un nuevo cliente");
        
        //Componentes para panel Centro:
        JLabel lbl1 = new JLabel("Nombre");
        JTextField tf1 = new JTextField(25);
        JLabel lbl2 = new JLabel("Dirección:");
        JTextField tf2 = new JTextField(25);
        JLabel lbl3 = new JLabel("Telefono");
        JTextField tf3 = new JTextField(25);
        JLabel lbl4 = new JLabel("Cuidad:");
        JTextField tf4 = new JTextField(25);
        
        String [] lista = {"Seleccione un estado", "Veracruz", "Sonora", "Tabasco", "Oaxaca", "Tamaulipas", "DF", "Nuevo Leon"};
        JLabel lbl5 = new JLabel("Estado:");
        JComboBox cb1 = new JComboBox(lista);
        
        //Componentes para el panel Sur
        JButton bs1 = new JButton("Registrar");
        bs1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                listaClients.addElement(tf1.getText() + " "+ 
                         tf2.getText() + " "+
                         tf3.getText() + " "+
                         tf4.getText() +" " +lista[cb1.getSelectedIndex()] );
                         
                tf1.setText("");
                tf2.setText("");
                tf3.setText("");
                tf4.setText("");
                
                listaClientes.setModel(listaClients);
                JOptionPane.showMessageDialog(null, "Registro Exitoso");
            }
        });
        JButton bs2 = new JButton("Cancelar");
        
        //Componentes para el panel Oeste:
        JButton bo1 = new JButton("Lista de Clientes");
        bo1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) { //Tratar de hacer aquí una clase con esto:
                JFrame ventana2 = new JFrame("Lista de clientes");
                ventana2.setDefaultCloseOperation(2);
                ventana2.setLocation(500,90);
                ventana2.setSize(500, 500);
                
                JPanel panelMayorClientes = new JPanel(new BorderLayout());
                JPanel pnlClientesCentro = new JPanel();
                GridBagLayout gblClientes = new GridBagLayout();
                GridBagConstraints gbClientes = new GridBagConstraints();
                pnlClientesCentro.setLayout(gblClientes);           
                
               gbClientes.gridx = 0;
               gbClientes.gridy = 0;
        
               gbClientes.anchor = GridBagConstraints.WEST;
               pnlClientesCentro.add(listaClientes, gbClientes);
               gbClientes.gridwidth = 1;
               gbClientes.gridy = 2;
               
    
               panelMayorClientes.add(pnlClientesCentro, BorderLayout.CENTER);
               ventana2.add(panelMayorClientes);
               ventana2.setVisible(true);
            }
        });
        
        
        panelNorte.add(lblTitulo);
        
        gbc1.gridx = 0;
        gbc1.gridy = 0;
        
        gbc1.anchor = GridBagConstraints.WEST;
        panelCentro.add(lbl1, gbc1);
        gbc1.gridy = 1;
        gbc1.gridwidth = 5;
        panelCentro.add(tf1,gbc1);
        gbc1.gridwidth = 1;
        gbc1.gridy = 2;
        panelCentro.add(lbl2,gbc1);
        gbc1.gridy = 3;
        gbc1.gridwidth = 8;
        panelCentro.add(tf2,gbc1);
        gbc1.gridwidth = 1;
        gbc1.gridy = 4;
        panelCentro.add(lbl3,gbc1);
        gbc1.gridy = 5;
        gbc1.gridwidth = 5;
        panelCentro.add(tf3,gbc1);
        gbc1.gridwidth = 1;
        gbc1.gridy = 6;
        panelCentro.add(lbl4,gbc1);
        gbc1.gridwidth = 5;
        gbc1.gridy = 7;
        panelCentro.add(tf4,gbc1);
        gbc1.gridwidth = 1;
        gbc1.gridy = 8;
        panelCentro.add(lbl5,gbc1);
        gbc1.gridwidth = 2;
        gbc1.gridy = 9;
        panelCentro.add(cb1, gbc1);
        
        panelSur.add(bs1);
        
        panelSur.add(bs2);
        
       // panelEste.add(be1);
       // panelEste.add(be2);
        
        panelOeste.add(bo1);
        //panelOeste.add(bo2);
        //panelOeste.add(bo3);
        
        //Integrar paneles a panelMayor:
        panelMayor.add(panelNorte, BorderLayout.NORTH);
        panelMayor.add(panelCentro, BorderLayout.CENTER);
        panelMayor.add(panelSur, BorderLayout.SOUTH);  
        panelMayor.add(panelEste, BorderLayout.EAST);
        panelMayor.add(panelOeste, BorderLayout.WEST);
        
        f.add(panelMayor);
        
        f.setVisible(true);
         
    }    
}
