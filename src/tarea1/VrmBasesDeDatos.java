
package tarea1;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;


/**
 *
 * @author Donato
 */
public class VrmBasesDeDatos extends JFrame{
    
    public VrmBasesDeDatos(){
        marco();
    }
    public void marco(){
        
         this.setSize(500,500);
         this.setLocation(200,100);
         this.setTitle("Base de Datos Disponibles");
         
         JPanel totalPanel = new JPanel(new BorderLayout()); 
         JPanel northPanel = new JPanel();
         JPanel centerPanel = new JPanel(); 
         JPanel southPanel = new JPanel();   
         
        FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER); 
        northPanel.setLayout(fl1); 
        
        GridBagLayout gbl1 = new GridBagLayout();
        centerPanel.setLayout(gbl1);
        GridBagConstraints gbc1 = new GridBagConstraints(); 
        gbc1.gridx =0;
        gbc1.gridy = 0;
        gbc1.anchor = GridBagConstraints.WEST;
       
        
        FlowLayout fl2 = new FlowLayout();
        southPanel.setLayout(fl2); 
                                           
                                    //componenntes panel centro
        JLabel select = new JLabel("Elegir un producto:  "); 
        centerPanel.add(select,gbc1); 
        gbc1.gridx=1;
        
        String[] all = {"Seleccione una opción", "Phoenix", "Oracle", "MySQL", "SQLServer"};
        JComboBox products = new JComboBox(all); 
        centerPanel.add(products,gbc1);
        gbc1.gridy = 2;
        
        JButton aac = new JButton("Agregar al carrito"); 
        centerPanel.add(aac,gbc1); 
        
        
        
        
        
        JButton comp = new JButton("Comprar");
        comp.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame ventana2 = new JFrame();
                ventana2.setDefaultCloseOperation(2);
                ventana2.setSize(500, 500);
                ventana2.setLocationRelativeTo(null);
                ventana2.setVisible(true);
                ventana2.setTitle("Metodos de pago");
            }
        });
        JButton can = new JButton("Cancelar");
        can.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {            
                
                
            }
        });
        southPanel.add(comp);
        southPanel.add(can);
        
        totalPanel.add(centerPanel, BorderLayout.CENTER); 
        totalPanel.add(southPanel, BorderLayout.SOUTH);  
         
         
         
         this.add(totalPanel);
         this.setVisible(true); 
    }
    
}
