
package tarea1;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author Donato
 */
public class FrmDesarrolladores extends JFrame{
    
    public FrmDesarrolladores(){
        marco();
    }
    public void marco(){
        this.setSize(400,400);
        this.setTitle("Informacion de los Desarrolladores del Sistema");
        this.setVisible(true);
        this.setLocationRelativeTo(null); 
        
        JPanel pnl1 = new JPanel();
        FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER); 
        JPanel panelCentro = new JPanel(new BorderLayout());
        JPanel panelNorte = new JPanel(); 
        
        JLabel lbl1 = new JLabel("Desarrolladores del sistema");
        JTextArea txa1 = new JTextArea("Ana Laura Jacome Gonzalez "
                                    + "\n Alexis Salazar Viveros "
                                    + "\nDonato García Romero");
        txa1.setEditable(false); 
        
        
        panelNorte.add(lbl1); 
    panelCentro.setLayout(fl1);    
    panelCentro.add(txa1); 
    pnl1.add(panelNorte);
    pnl1.add(panelCentro);
    
    this.add(pnl1);
    
                
    }    
    
}
